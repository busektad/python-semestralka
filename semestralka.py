#!/usr/bin/env python3

from PIL import ImageTk
from PIL import Image as img
import numpy as np
import sys
from tkinter import *

def DisplayPicture (obr, label):
    im1 = img.fromarray(obr)
    t_img = ImageTk.PhotoImage(im1)
    label.configure(image=t_img)
    label.image=t_img
    label.pack(side='bottom')
    

def mirror (obr, label):
    obr1 = np.flip(obr, 1)
    
    DisplayPicture(obr1, label)
    #return obr1

def rotaten (obr, c, label):
    if c == 1:
        obr1 = np.rot90(obr, 3)
        DisplayPicture(obr1, label)
        #return obr1 
        
    if c == 3:
        obr1 = np.rot90(obr)
        DisplayPicture(obr1, label)
        #return obr1
        
       
def rotate (obr, c, dire, label):
    c = c % 4
    if c == 0:
        return obr
    if c == 2:
        obr1 = np.flip(obr, 0)
        return obr1
    if dire == 'l':
        c = (c+2) % 4
        obr1 = rotaten(obr, c, label)
        return obr1
    if dire == 'p':
        obr1 = rotaten(obr, c, label)
        return obr1

def negativ1 (obr):
     obr = 255 - obr
     return obr
        
def negativ (obr, label):
    #arr = [255, 255, 255]
    #for x in range(0, obr.shape[0]):
    #    for y in range(0, obr.shape[1]):
    #        for z in range(0, obr.shape[2]):
    #            obr[x][y][z] = 255 - obr[x][y][z]
             #obr[x][y] = arr - obr[x][y]
    obr = 255 - obr
    DisplayPicture(obr, label)
    #return obr
    
def ShadesOfGray (obr, label):
    #for x in range(0, obr.shape[0]):
    #    for y in range(0, obr.shape[1]):
    #        if(obr.shape[2] < 3):
    #            return obr
    #        obr[x][y][0] = obr[x][y][0] * 0.2989
    #        obr[x][y][1] = obr[x][y][1] * 0.5870
    #        obr[x][y][2] = obr[x][y][2] * 0.1140
    
    arr = [0.2989, 0.5870, 0.1140]
    obr = obr * arr
    obr = np.sum(obr, axis=2)
    DisplayPicture(obr, label)
    #return obr
            
def Light (obr, label):
    obr = obr + negativ1(obr)//3
    DisplayPicture(obr, label)
    #return obr
def Dark (obr, label):
    obr = obr//2
    DisplayPicture(obr, label)
    #return obr
                
def HighlightEdges (obr, label):
    cop = np.copy(obr)
    #soused = 0
    #pricteni sosuseda nahore
    #soused = soused[1:] + obr[:-1]
    #pricteni sosuseda vpravo
    #soused[:,:-1] +=  obr[:,1:]
    #pricteni sosuseda dole
    #soused[:-1] += obr[1:]
    #pricteni sosuseda vlevo
    #soused[:,1:] += obr[:,:-1]
    #pricteni sosuseda nahore vpravo
    #soused[1:,:-1] += obr[:-1,1:]
    #pricteni sosuseda dole vpravo
    #soused[:-1,:-1] += obr[1:,1:]
    #pricteni sosuseda dole vlevo
    #soused[:-1,1:] += obr[1:,:-1]
    #pricteni sosuseda nahore vlevo
    #soused[1:,1:] += obr[:-1,:-1]
    
    #obr = obr + (9*obr - soused)
    
    for x in range(1, (obr.shape[0] - 1)):
        for y in range(1, obr.shape[1] - 1):
            for z in range(0, obr.shape[2]):
                cop = (
                    + 9 * obr[x, y, z] #orig
                    - obr[x - 1, y, z] #vlevo
                    - obr[x + 1, y, z] #vpravo
                    - obr[x, y + 1, z] #nahore
                    - obr[x, y - 1, z] #dole
                    - obr[x - 1, y + 1, z]  #vlevonahore
                    - obr[x - 1, y - 1, z]  #vlevodole
                    - obr[x + 1, y - 1, z]  #vpravonahore
                    - obr[x + 1, y + 1, z]) #vpravodole
    
                #obr[x][y][z] =  cop
                if cop < 0:
                    cop = 0
                if cop > 255:
                    cop = 255
                obr[x][y][z] =  cop
    
                #if obr[x][y][z] < 0:
                #    obr[x][y][z] = 0
                #if obr[x][y][z] > 255:
                #    obr[x][y][z] = 255
    
    DisplayPicture(obr, label)
    #return cop


def main():    
    root = Tk()
    root.title('Photo')

    im = img.open(sys.argv[1])
    arr = np.array(im)

    t_img = ImageTk.PhotoImage(im)
    label = Label(root, image=t_img)
    label.pack(side='bottom')
    Button(root, text='ORIGINAL', width=10, command=lambda:DisplayPicture(arr, label)).pack(side='right')
    Button(root, text='MIRROR', width=10, command=lambda:mirror(arr, label)).pack(side='right')
    Button(root, text='ROTATE R', width=10, command=lambda:rotate(arr, 1, 'p', label)).pack(side='right')
    Button(root, text='ROTATE L', width=10, command=lambda:rotate(arr, 1, 'l', label)).pack(side='right')
    Button(root, text='INVERT', width=10, command=lambda:negativ(arr, label)).pack(side='left')
    Button(root, text='GRAY', width=10, command=lambda:ShadesOfGray(arr, label)).pack(side='left')
    Button(root, text='LIGHT', width=10, command=lambda:Light(arr, label)).pack(side='left')
    Button(root, text='DARK', width=10, command=lambda:Dark(arr, label)).pack(side='left')
    Button(root, text='EDGES', width=10, command=lambda:HighlightEdges(arr, label)).pack(side='left')
    #im1 = img.fromarray(arr1)
    #im1.show()
    root.mainloop()
    
if __name__ == '__main__':
    main()
